# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from image_cropping import ImageCroppingMixin

from models import Blog


class BlogAdmin(ImageCroppingMixin, ModelAdmin):
    list_display = ('title', 'image_tag', 'published', 'created')
    list_filter = ('published',)
    ordering = ['-created']


admin.site.register(Blog, BlogAdmin)

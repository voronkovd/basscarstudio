# -*- coding: utf-8 -*-
from django.db.models import TextField, DateField
from image_cropping import ImageRatioField

from app.models import AbstractAppModel


class Blog(AbstractAppModel):
    anons = TextField(max_length=1000, verbose_name=u'Анонс')
    cropping = ImageRatioField('image', '870x580', verbose_name=u'Пропорции')
    created = DateField(editable=False, auto_now_add=True,
                        verbose_name=u'Дата добавления')

    class Meta:
        db_table = 'blogs'
        verbose_name = u'Статью'
        verbose_name_plural = u'Статьи'
        ordering = ['-created']

    def get_absolute_url(self):
        return '/blog/%s' % str(self.pk)

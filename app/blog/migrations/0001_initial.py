# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields
import sirtrevor.fields

import app.utils


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False,
                    auto_created=True, primary_key=True)
                 ),
                ('title', models.CharField(
                    max_length=128,
                    verbose_name=u'Заголовок')
                 ),
                ('content', sirtrevor.fields.SirTrevorField(
                    verbose_name=u'Текст')
                 ),
                ('image', models.ImageField(
                    upload_to=app.utils.upload_to, verbose_name=u'Изображение')
                 ),
                ('published', models.BooleanField(
                    default=False, verbose_name=u'Отображать')
                 ),
                ('anons', models.TextField(
                    max_length=1000, verbose_name=u'Анонс')
                 ),
                (b'cropping',
                 image_cropping.fields.ImageRatioField(
                     b'image', '870x580', hide_image_field=False,
                     size_warning=True, allow_fullsize=False, free_crop=False,
                     adapt_rotation=False, verbose_name=u'Пропорции')
                 ),
                ('created', models.DateField(
                    auto_now_add=True, verbose_name='Дата добавления')
                 ),
            ],
            options={
                'ordering': ['-created'],
                'db_table': 'blogs',
                'verbose_name': u'Статью',
                'verbose_name_plural': u'Статьи',
            },
        ),
    ]

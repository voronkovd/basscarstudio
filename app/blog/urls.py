# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

from views import blog_detail, blog_list

urlpatterns = patterns('',
                       url(r'^index/$', blog_list, name='blog_list'),
                       url(r'^(?P<pk>[\w-]+)$', blog_detail,
                           name='blog_detail'),
                       )

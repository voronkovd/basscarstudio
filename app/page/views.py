# coding: utf-8
from django.views import generic

from forms import ContactForm


class Contact(generic.FormView):
    form_class = ContactForm
    success_url = '/page/contact/'
    template_name = 'page/contact.html'

    def form_valid(self, form):
        form.save()
        return super(Contact, self).form_valid(form)


contact = Contact.as_view()

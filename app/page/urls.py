# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

from views import contact

urlpatterns = patterns('',
                       url(r'^contact/$', contact, name='page_contact'),
                       )

# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template import Context, loader


class ContactForm(forms.Form):
    name = forms.CharField(max_length=36)
    phone = forms.CharField(max_length=16)
    email = forms.EmailField()
    content = forms.Textarea()

    def save(self):
        c = Context({
            'name': self.cleaned_data['name'],
            'phone': self.cleaned_data['name'],
            'email': self.cleaned_data['name'],
            'content': self.cleaned_data['name'],
        })
        text_content = loader.render_to_string('page/mail/contact.txt', c)
        html_content = loader.render_to_string('page/contact.html', c)
        email = EmailMultiAlternatives(
            u'Для вас есть сообщение',
            text_content,
            settings.DEFAULT_FROM_EMAIL)
        email.attach_alternative(html_content, "text/html")
        email.to = [settings.CONTACT_EMAIL_TO]
        email.send()

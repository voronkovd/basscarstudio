# -*- coding: utf-8 -*-
import uuid


def upload_to(instance, filename):
    h = str(uuid.uuid4().hex)
    folder = str(instance.__class__.__name__).lower()
    return '{}/{}/{}/{}.{}'.format(folder, h[:2], h[2:4], h,
                                   filename.split('.')[-1].lower())

# -*- coding: utf-8 -*-
import os
import ConfigParser
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
from easy_thumbnails.conf import Settings as thumbnail_settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

my_config = ConfigParser.ConfigParser()
my_config.readfp(open(BASE_DIR + '/app/settings.ini'))

SECRET_KEY = my_config.get('main', 'secret_key')
DEBUG = my_config.getboolean('main', 'debug')
TEMPLATE_DEBUG = my_config.getboolean('main', 'debug')
ADMINS = ((my_config.get('main', 'admin_name'),
           my_config.get('main', 'admin_email')),)
ALLOWED_HOSTS = ['*']
ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'
LANGUAGE_CODE = 'ru-Ru'
TIME_ZONE = 'Asia/Novosibirsk'
USE_I18N = True
USE_L10N = True
USE_TZ = True

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    "django.core.context_processors.request",
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
)


# utils
INSTALLED_APPS = (
    'easy_thumbnails',
    'image_cropping',
    'suitlocale',
    'suit',
    'sirtrevor',
)

# django
INSTALLED_APPS += (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

# apps
INSTALLED_APPS += (
    'app',
    'slider',
    'blog',
    'work',
    'page'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': my_config.get('mysql', 'name'),
        'USER': my_config.get('mysql', 'user'),
        'PASSWORD': my_config.get('mysql', 'password'),
        'HOST': my_config.get('mysql', 'host'),
    }
}
TEMPLATE_DIRS = (BASE_DIR + '/templates/',)

STATIC_ROOT = BASE_DIR + '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR + '/media/'
STATICFILES_DIRS = ()
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_PASSWORD = my_config.get('email', 'password')
EMAIL_HOST_USER = my_config.get('email', 'user')
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_PORT = 465
EMAIL_USE_SSL = True
CONTACT_EMAIL_TO = 'all@basscarstudio.ru'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        }
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}

SUIT_CONFIG = {
    'ADMIN_NAME': 'Админка',
    'SEARCH_URL': '',
    'HEADER_DATE_FORMAT': 'd.m.Y',
    'HEADER_TIME_FORMAT': 'H:i',
    'MENU_OPEN_FIRST_CHILD': True,
}

THUMBNAIL_PROCESSORS = (
                           'image_cropping.thumbnail_processors.crop_corners',
                       ) + thumbnail_settings.THUMBNAIL_PROCESSORS
IMAGE_CROPPING_SIZE_WARNING = True

SIRTREVOR_BLOCK_TYPES = ['Text', 'List', 'Quote', 'Image', 'Video', 'Heading']

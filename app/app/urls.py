# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import GenericSitemap

from app import views
from app.sitemap import work_dict, blog_dict

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', views.index, name='app_index'),
                       url(r'^page/', include('page.urls')),
                       url(r'^work/', include('work.urls')),
                       url(r'^blog/', include('blog.urls')),
                       url(r'^sirtrevor/', include('sirtrevor.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^sitemap\.xml$', sitemap,
                           {'sitemaps':
                                {'work': GenericSitemap(work_dict,
                                                        priority=0.9),
                                 'blog': GenericSitemap(blog_dict,
                                                        priority=0.6)}
                            },
                           name='django.contrib.sitemaps.views.sitemap'),
                       (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT}),
                       (r'^static/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.STATIC_ROOT}),
                       )

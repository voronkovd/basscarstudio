# coding: utf-8
from django.views import generic


class Index(generic.TemplateView):
    template_name = 'index.html'


index = Index.as_view()

# coding: utf-8
from django.template import Library

from slider.models import Slider

register = Library()


@register.inclusion_tag('slider/list.html')
def slider_view():
    sliders = Slider.objects.filter(published=True).all()
    return {'sliders': sliders}

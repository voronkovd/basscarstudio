# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sirtrevor.fields

import app.utils


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Work',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False,
                    auto_created=True, primary_key=True)
                 ),
                ('title', models.CharField(
                    max_length=128,
                    verbose_name=u'Заголовок')
                 ),
                ('content', sirtrevor.fields.SirTrevorField(
                    verbose_name=u'Текст')
                 ),
                ('image', models.ImageField(
                    upload_to=app.utils.upload_to, verbose_name=u'Изображение')
                 ),
                ('published', models.BooleanField(
                    default=False, verbose_name=u'Отображать')
                 ),
                ('order', models.PositiveIntegerField(
                    null=True, verbose_name=u'Сортировка')
                 ),
            ],
            options={
                'ordering': ['order'],
                'db_table': 'works',
                'verbose_name': u'Работу',
                'verbose_name_plural': u'Работы',
            },
        ),
        migrations.CreateModel(
            name='WorkImage',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False,
                    auto_created=True, primary_key=True)
                 ),
                ('image', models.ImageField(
                    upload_to=app.utils.upload_to,
                    verbose_name=u'Работа')
                 ),
                ('work', models.ForeignKey(
                    verbose_name=u'Изображение', to='work.Work')
                 ),
            ],
            options={
                'db_table': 'work_images',
                'verbose_name': u'Фотографию',
                'verbose_name_plural': u'Фотографии',
            },
        ),
    ]

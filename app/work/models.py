# -*- coding: utf-8 -*-
from django.db import models

from app.models import AbstractAppModel
from app.utils import upload_to as to


class Work(AbstractAppModel):
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)

    class Meta:
        db_table = 'works'
        verbose_name = u'Работу'
        verbose_name_plural = u'Работы'
        ordering = ['order']

    def gallery(self):
        return self.workimage_set.all()

    def get_absolute_url(self):
        return '/work/%s' % str(self.pk)


class WorkImage(models.Model):
    work = models.ForeignKey(Work, verbose_name=u'Работа')
    image = models.ImageField(upload_to=to, verbose_name=u'Изображение')

    class Meta:
        db_table = 'work_images'
        verbose_name = u'Фотографию'
        verbose_name_plural = u'Фотографии'
